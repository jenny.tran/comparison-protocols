function [Preview] = f_imoverlayAlpha(Im, Mask, Color, Alpha)
%UNTITLED4 Summary of this function goes here

    %ColorBlend = zeros(size(Im, 1), size(Im, 2), size(Im, 3), 'uint16');
    ColorBlend = zeros(size(Im, 1), size(Im, 2), size(Im, 3), 'uint8');
    for i = 1:3 % RGB loop
        %ColorBlend(:,:,i) = uint8(round(Color(i) * Alpha * 255)) * uint8(Mask);
        %ColorBlend(:,:,i) = uint16(double(round(Color(i) * Alpha * (2^16 -1))) * double(Mask));
        ColorBlend(:,:,i) = uint8(double(round(Color(i) * Alpha * (2^8 -1))) * double(Mask));
    end
    %imtool(ColorBlend)
    if length(size(Im)) == 2
        Im = cat(3, Im, Im, Im);
    end
    if strcmp(class(Im), 'uint16')
        Im = im2uint8(Im);
    end
    
    Preview = Im + ColorBlend; %imtool(Preview)
end

