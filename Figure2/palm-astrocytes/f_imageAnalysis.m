function [GFAPperNucObjects, ObjectsPerField] = f_imageAnalysis(chNucall, chGFAPall, WellThis, FieldThis, MesFile, PreviewPath, ThresholdGFAP)
%Summary is fine do previews and clean comments
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    

    %% segment nuclei
    chNuc = max(chNucall, [], 3); % imtool(chNuc, [])
    NucLP = imfilter(chNuc, fspecial('gaussian', 15, 3), 'symmetric'); % imtool(NucLP,[])
    %% Dynamic nuclei thresholding
    NucThreshold = getNucThreshold(0, max(NucLP(:)), 300, NucLP);
    NucMaskAll = NucLP > NucThreshold; %imtool(NucMaskAll,[])
    %% Close holes
    NucMaskAll = imfill(NucMaskAll,'holes');
    %%
    
% % %     %% Nuclei Splitting by shape
% % %     NucSplitD = bwdist(~NucMaskAll);% imtool(NucSplitD, [])
% % %     NucSplitW = watershed(imhmin(im2uint16(mat2gray(imcomplement(NucSplitD))),500));% imtool(im2uint16(mat2gray(imcomplement(NucSplitD))))
% % %     NucStencil = uint16(NucMaskAll) .* uint16(NucSplitW);
% % %     % imtool(NucSplitW,[])
% % %     % imtool(NucStencil,[])

    %% End of updated block
    NucMaskAll = bwareaopen(NucMaskAll, 5000);%imtool(NucMaskAll,[])
    NucMask = bwareafilt(NucMaskAll, [5000, 50000]);%imtool(NucMask,[])
    [NucLM, NucCount] = bwlabeln(NucMask, 4); %imtool(NucLM,[])
    NucObjects = regionprops('table', NucLM, chNuc, {'MeanIntensity','Area','MajorAxisLength','MinorAxisLength','Perimeter'});
    NucSummary = varfun(@(x) mean(x), NucObjects);
    NucSummary.SumArea = sum(NucMask(:));
    NucSummary.Count = NucCount;
    NucSummary.Properties.VariableNames = strcat('Nuc_', NucSummary.Properties.VariableNames);

    %% GFAP with feature extraction at single cell level
    D = bwdist(NucMask);% imtool(D, [])
    W = watershed(D);% imtool(W, [])

    NucExtended = uint16(imdilate(NucMask, strel('disk', 15))) .* uint16(W); % imtool(NucExtended, [])
    Perinuc = NucExtended .* uint16(~NucMask);% imtool(Perinuc, [])
    PerinucMask = logical(Perinuc);
    
    chGFAP = max(chGFAPall, [], 3); % imtool(chGFAP, [])
    ObjectsPeriNucGFAPid = regionprops('table', Perinuc, Perinuc, {'MeanIntensity'});%Label Matrix
    ObjectsPeriNucGFAPMean = regionprops('table', Perinuc, chGFAP, {'MeanIntensity'});%GFAP

    if false
        figure
        histogram(ObjectsPeriNucGFAPMean.MeanIntensity, 20)
    end

    GFAPperNucObjects = regionprops('table', Perinuc, {'PixelIdxList'});
    for i = 1:height(GFAPperNucObjects)
        GFAPperNucObjects(i, 'QuantileGFAP_025') = {quantile(chGFAP(GFAPperNucObjects{i, 'PixelIdxList'}{:}), 0.25)};
        GFAPperNucObjects(i, 'QuantileGFAP_050') = {quantile(chGFAP(GFAPperNucObjects{i, 'PixelIdxList'}{:}), 0.50)};
        GFAPperNucObjects(i, 'QuantileGFAP_075') = {quantile(chGFAP(GFAPperNucObjects{i, 'PixelIdxList'}{:}), 0.75)};
        GFAPperNucObjects(i, 'GFAPClass') = {uint8(GFAPperNucObjects{i, 'QuantileGFAP_075'} > ThresholdGFAP)};
    end
    GFAPperNucObjects= GFAPperNucObjects(:, 2:end);
    GFAPperNucObjects.ID = table2cell(ObjectsPeriNucGFAPid);
    GFAPperNucObjects.MeanIntensity = table2cell(ObjectsPeriNucGFAPMean);
    Celldummy = cell(height(GFAPperNucObjects), 1);
    GFAPperNucObjects.Well = cellfun(@(x) WellThis, Celldummy,  'UniformOutput', false);
    GFAPperNucObjects.Field = cellfun(@(x) FieldThis, Celldummy,  'UniformOutput', false);
    
    
    %% Analysis per cell
    GFAPMask = medfilt2(chGFAP) > 500; % imtool(GFAPMask,[])
    GFAPMask = bwareaopen(GFAPMask, 2500);
    GFAPLM = uint16(GFAPMask) .* uint16(W); % imtool(GFAPLM, []);
    GFAPCentralMask = imreconstruct(NucMask, logical(GFAPLM)); % keep those touching a nucleus
    
    %% remove black spots
    GFAPCentralMask = ~f_RemoveSmallObjects(~GFAPCentralMask,500);%imtool(GFAPCentralMask)
    GFAPMask = GFAPCentralMask;
    
    %% segment cells (section to be applied to other script ...)
    chGFAP = max(chGFAPall, [], 3); % imtool(ch3, [])
    ch3TopHat = imtophat(chGFAP, strel('disk', 101)); % imtool(ch3TopHat, [])
    GFAP_TopHat_Mask = ch3TopHat > 750;% imtool(GFAP_TopHat_Mask)
    GFAPMaskFine = GFAP_TopHat_Mask | GFAPMask | NucMask; % imtool(GFAPMaskFine,[])
    GFAPMaskFine = ~f_RemoveSmallObjects(~GFAPMaskFine, 500);
    GFAPSomaMask = imopen(GFAPMaskFine, strel('disk', 31)); % imtool(GFAPSomaMask + GFAPMaskFine + 3*NucMask,[]) imtool(GFAPSomaMask + 3*NucMask,[])
    GFAPSomaMask = imreconstruct(NucMask, GFAPSomaMask);
    GFAPSomaMask = GFAPSomaMask & GFAPMaskFine;
    ProtrusionMask = GFAPMaskFine & ~GFAPSomaMask;% imtool(ProtrusionMask,[])
    
    ProtrusionMask = imreconstruct(imdilate(GFAPSomaMask, strel('disk', 1)), ProtrusionMask);
    
    ProtrusionPerimeterMask = bwperim(ProtrusionMask);%imtool(ProtrusionPerimeterMask,[])
    GFAPProtrusionSkel = bwmorph(ProtrusionMask, 'skel', inf);% imtool(GFAPProtrusionSkel,[])

    %% Find nuclei in GFAPSomaMask
    NucGFAP = imreconstruct(GFAPSomaMask, NucMask); % imtool(NucGFAP, [])
    
    %% Extract features per field
    ObjectsPerField = table();
    ObjectsPerField.Well = {WellThis};
    ObjectsPerField.Field = {FieldThis};
    ObjectsPerField.Mes = {MesFile};
    [~, ObjectsPerField.CountNuclei] = bwlabeln(NucMask, 4);
    [~, ObjectsPerField.CountNucGFAP] = bwlabeln(NucGFAP, 4);
    [~, ObjectsPerField.CountNucleiAll] = bwlabeln(NucMaskAll);
    ObjectsPerField.AreaNuclei = sum(NucMask(:));
    ObjectsPerField.AreaNucleiAll = sum(NucMaskAll(:));
    ObjectsPerField.AreaProtrusionsAll = sum(ProtrusionMask(:));
    ObjectsPerField.PerimeterProtrusions = sum(ProtrusionPerimeterMask(:));
    ObjectsPerField.ProtrusionsPerimeterByArea = ObjectsPerField.PerimeterProtrusions / ObjectsPerField.AreaProtrusionsAll;
    %ObjectsPerField.GFAPMaskFineArea = sum(GFAPMaskFine(:)); % GFAP mask per field is more detailed but less split than the per cell version
    %% ObjectsPerField.GFAPSomaMaskArea = sum(GFAPMask(:)); % GFAP mask per field is more detailed but less split than the per cell version
    ObjectsPerField.GFAPSomaMaskArea = sum(GFAPSomaMask(:)); % GFAP mask per field is more detailed but less split than the per cell version
    %%% ObjectsPerField.ProtrusionAreaBySomaArea = ObjectsPerField.AreaProtrusionsAll / ObjectsPerField.GFAPMaskFineArea;
    ObjectsPerField.ProtrusionAreaBySomaArea = ObjectsPerField.AreaProtrusionsAll / ObjectsPerField.GFAPSomaMaskArea;

    ObjectsPerField.SkelArea = sum(GFAPProtrusionSkel(:));
    ObjectsPerField.SkelAreaByProtrusionArea =  ObjectsPerField.SkelArea / ObjectsPerField.AreaProtrusionsAll;% How fine are the protrusions
 
    %% Previews
    imSize = size(chNuc);
    [BarMask, ~] = f_barMask(20, 0.10758027143330025, imSize, imSize(1)-50, 50, 20);% 20 micrometers
    %it(BarMask)
    
    RGBPreview = cat(3, imadjust(chGFAP, [0 0.05], [0 1]), zeros(size(chGFAP), 'uint16'), imadjust(chNuc, [0 0.01], [0 1]));
    RGBPreview = f_imoverlayIris(RGBPreview, BarMask, [1 1 1]);
    %imtool(RGBPreview)
    
    %NucPreview = f_imoverlayIris(imadjust(chNuc, [0 0.01], [0 1]), imdilate(bwperim(NucMaskAll),strel('disk', 1)), [0 0 1]);
    NucPreview = f_imoverlayAlpha(imadjust(chNuc, [0 0.01], [0 1]), ProtrusionMask, [1 0 0], 0.05);
    NucPreview = f_imoverlayAlpha(NucPreview, GFAPSomaMask, [0 1 0], 0.05);
    NucPreview = f_imoverlayIris(NucPreview, imdilate(bwperim(NucMask),strel('disk', 1)), [0 1 1]);
    NucPreview = f_imoverlayIris(NucPreview, BarMask, [1 1 1]);
    %imtool(NucPreview)
    
    PerinucGFAPpreview = f_imoverlayIris(imadjust(chGFAP, [0 0.1], [0 1]), bwperim(PerinucMask), [1 0 0]);
    PerinucGFAPpreview = f_imoverlayIris(PerinucGFAPpreview, BarMask, [1 1 1]);
    %imtool(PerinucGFAPpreview)
    
    ProcessPreview = f_imoverlayIris(imadjust(chGFAP, [0 0.1], [0 1]), ProtrusionPerimeterMask, [1 1 0]);
    ProcessPreview = f_imoverlayAlpha(ProcessPreview, ProtrusionMask, [1 0 0], 0.2);
    ProcessPreview = f_imoverlayAlpha(ProcessPreview, GFAPSomaMask, [0 1 0], 0.2);
    ProcessPreview = f_imoverlayIris(ProcessPreview, bwperim(NucMask), [0 0 1]);
    ProcessPreview = f_imoverlayIris(ProcessPreview, bwperim(NucGFAP), [0 1 1]);
    ProcessPreview = f_imoverlayIris(ProcessPreview, GFAPProtrusionSkel, [1 0 0]);
    ProcessPreview = f_imoverlayIris(ProcessPreview, BarMask, [1 1 1]);
    %imtool(ProcessPreview)

    RGBPreviewPath = [PreviewPath, filesep, WellThis, '_', FieldThis, '_RGB.png'];
    NucPreviewPath = [PreviewPath, filesep, WellThis, '_', FieldThis, '_Nuc.png'];
    PerinucGFAPPath = [PreviewPath, filesep, WellThis, '_', FieldThis, '_PerinucGFAP.png'];
    ProcessPath = [PreviewPath, filesep, WellThis, '_', FieldThis, '_ProcessGFAP.png'];
    
    imwrite(RGBPreview, RGBPreviewPath)
    imwrite(NucPreview, NucPreviewPath)
    imwrite(PerinucGFAPpreview, PerinucGFAPPath)
    imwrite(ProcessPreview, ProcessPath)

end

