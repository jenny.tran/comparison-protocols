function [Threshold] = getNucThreshold(Min, Max, Steps, NucIm)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

    MetricVec = [];
    DeltaVec = [];
    Candidates = uint16(linspace(double(Min), double(Max), double(Steps)));%[Min:Steps:Max];
    for i = 1:Steps
        Mask = NucIm > Candidates(i);
        MetricVec(i) = sum(Mask(:));
        if i == 1
            DeltaVec(i) = 0;
        else
            DeltaVec(i) = MetricVec(i) - MetricVec(i-1);
        end
    end
    % figure; plot(DeltaVec)
    
    % clip values that do not detect anything or very little
    DeltaVecCorr = DeltaVec(find(DeltaVec == min(DeltaVec)): end);%  figure; plot(DeltaVecCorr)
    CandidatesCorr = Candidates(find(DeltaVec == min(DeltaVec)): end);

    % Cut off dynamically
    Threshold = quantile(CandidatesCorr, 0.15);

end

